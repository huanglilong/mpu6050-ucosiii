#include "stm32f4_i2c_mpu6050.h"
#define I2Cx I2C1
/**
  * @brief  Enables the I2C Clock and configures the different GPIO ports.
  * @param  None
  * @retval None
  */
void I2C_GPIO_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  I2C_InitTypeDef I2C_InitStruct;
  
  // enable APB1 peripheral clock for I2C1
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
  // enable clock for SCL and SDA pins
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  
  /* setup SCL and SDA pins
   * You can connect I2C1 to two different
   * pairs of pins:
   * 1. SCL on PB6 and SDA on PB7 
   * 2. SCL on PB8 and SDA on PB9
   */
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;   // we are going to use PB6 and PB7
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;	        // set pins to alternate function
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;	// set GPIO speed
  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;		// set output to open drain --> the line has to be only pulled low, not driven high
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;		// enable pull up resistors
  GPIO_Init(GPIOB, &GPIO_InitStruct);			// init GPIOB
  
  // Connect I2C1 pins to AF  
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1); // SCL
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_I2C1); // SDA
  
  // configure I2C1 
  I2C_InitStruct.I2C_ClockSpeed = 400000; 		// 400kHz
  I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;		// I2C mode
  I2C_InitStruct.I2C_DutyCycle = I2C_DutyCycle_2;	// 50% duty cycle --> standard
  I2C_InitStruct.I2C_OwnAddress1 = 0x00;		// own address, not relevant in master mode
  I2C_InitStruct.I2C_Ack = I2C_Ack_Enable;		// disable acknowledge when reading (can be changed later on)
  I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit; // set address length to 7 bit addresses
  I2C_Init(I2C1, &I2C_InitStruct);			// init I2C1
  I2C_AcknowledgeConfig(I2C1, DISABLE);
  // enable I2C1
  I2C_Cmd(I2C1, ENABLE);
}

/*************************************************
* brief         : I2C protocl --- frist part is standard I2C, 
                              --- second part is for MPU6050
* time          : 2015/7/31
* author        : huanglilong
**************************************************/
/* standrad part */
void I2C_Start(void)
{
  I2C_GenerateSTART(I2Cx, ENABLE);      /* I2C start condition */
}
void I2C_Stop(void)
{
  I2C_GenerateSTOP(I2Cx, ENABLE);       /* I2C stop condition */
}
void I2C_SendByte(unsigned char SendByte)
{
  I2C_SendData(I2Cx, SendByte);         /* I2C send uint8_t data */
}
unsigned char I2C_ReadByte(void)
{
  return I2C_ReceiveData(I2Cx);         /* I2C read uint8_t data */
}
void I2C_SendAddress(unsigned char Address, unsigned char Direction)
{
  I2C_Send7bitAddress(I2Cx, Address, Direction);
}

/* second part for MPU6050 */

/*****************************
* Single-Byte Write Sequence 
*/
void I2C_Write(unsigned char SlaveAddress, unsigned char REG_Address, unsigned char REG_data)
{
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));                                /* wait for I2C */
  
  I2C_Start();                                                                  /* start */
  while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));                  /* EV5 */
  
  I2C_SendAddress(SlaveAddress, I2C_Direction_Transmitter);                     /* write mode */
  while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));     /* EV6 and clear flag */
  
  I2C_SendByte(REG_Address);                                                    /* send MPU6050 register's address */
  while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTING));             /* EV8 -- byte has been transmitting */
  
  I2C_SendByte(REG_data);                                                       /* write data to MPU6050 register */
  while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));              /* EV8_2 -- byte has been transmitted */
  
  I2C_Stop();
  
}
unsigned char I2C_Read(unsigned char SlaveAddress,unsigned char REG_Address)
{
  unsigned char REG_data =0;
  
  while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));
  
  I2C_Start();                                                                  /* start */
  while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));                  /* EV5 */
  
  I2C_SendAddress(SlaveAddress, I2C_Direction_Transmitter);                     /* write mode */
  while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));     /* EV6 */
  
  I2C_SendByte(REG_Address);                                                    /* send MPU6050 register's address */
  while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));              /* EV8_2 -- byte has been transmitted */
  I2C_Stop();
  
  I2C_Start();                                                                  /* start */ 
  while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));                  /* EV5 */
  
  I2C_SendAddress(SlaveAddress, I2C_Direction_Receiver);                        /* read mode */
  while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));        /* EV6 */
  
  while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED));                 /* EV7 */
  REG_data = I2C_ReadByte();                                                    /* received data */
  
  I2C_Stop();
  return REG_data;
}
void I2C_writeBits(unsigned char dev,unsigned char reg,unsigned char bitStart,unsigned char length,unsigned char data)
{
    unsigned char b,mask;
    b = I2C_Read(dev, reg);
    mask = (0xFF << (bitStart + 1)) | 0xFF >> ((8 - bitStart) + length - 1);
    data <<= (8 - length);
    data >>= (7 - bitStart);
    b &= mask;
    b |= data;
    I2C_Write(dev,reg,b);					 
}
void I2C_writeBit(unsigned char dev, unsigned char reg, unsigned char bitNum, unsigned char data)
{
  unsigned char b;
  b = I2C_Read(dev, reg);
  b = (data != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum));
  I2C_Write(dev,reg,b);	
}



