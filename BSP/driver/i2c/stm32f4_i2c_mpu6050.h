#ifndef __STM32F4_I2C_H
#define __STM32F4_I2C_H
#include "stm32f4xx.h"

void I2C_GPIO_Config(void);
void I2C_Start(void);
void I2C_Stop(void);
void I2C_SendByte(unsigned char SendByte);
unsigned char I2C_ReadByte(void);
void I2C_Write(unsigned char SlaveAddress,unsigned char REG_Address,unsigned char REG_data);
unsigned char I2C_Read(unsigned char SlaveAddress,unsigned char REG_Address);
void I2C_writeBits(unsigned char dev,unsigned char reg,unsigned char bitStart,unsigned char length,unsigned char data);
void I2C_writeBit(unsigned char dev, unsigned char reg, unsigned char bitNum, unsigned char data);


#endif

