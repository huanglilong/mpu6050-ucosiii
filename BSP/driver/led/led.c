/*****************************************
*@file      : led.c
*@brief     : define led's API
*@time      : 2014/10/6
*@author    : jom
*******************************************/

#include "led.h"
/*******************************************
*@brief        : for led GPIO init
*@param        : none
*@return       : none
*******************************************/
void ledInit(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  /* GPIOG Peripheral clock enable */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

  /* Configure PG6 and PG8 in output pushpull mode */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);

}
/*********************************************
  *@brief     : led on
  *@param     : none
  *@return    : none
**********************************************/
void ledOn(void)
{
    GPIOD->BSRRL = GPIO_Pin_12;  
}
/*********************************************
  *@brief     : led off
  *@param     : none
  *@return    : none
**********************************************/
void ledOff(void)
{
  GPIOD->BSRRH = GPIO_Pin_12;
}
/*********************************************
  *@brief     : led on
  *@param     : none
  *@return    : none
**********************************************/
void ledOn1(void)
{
    GPIOD->BSRRL = GPIO_Pin_13;  
}
/*********************************************
  *@brief     : led off
  *@param     : none
  *@return    : none
**********************************************/
void ledOff1(void)
{
  GPIOD->BSRRH = GPIO_Pin_13;
}

/*********************************************
  *@brief     : led on
  *@param     : none
  *@return    : none
**********************************************/
void ledOn2(void)
{
    GPIOD->BSRRL = GPIO_Pin_14;  
}
/*********************************************
  *@brief     : led off
  *@param     : none
  *@return    : none
**********************************************/
void ledOff2(void)
{
  GPIOD->BSRRH = GPIO_Pin_14;
}

/*********************************************
  *@brief     : led on
  *@param     : none
  *@return    : none
**********************************************/
void ledOn3(void)
{
    GPIOD->BSRRL = GPIO_Pin_15;  
}
/*********************************************
  *@brief     : led off
  *@param     : none
  *@return    : none
**********************************************/
void ledOff3(void)
{
  GPIOD->BSRRH = GPIO_Pin_15;
}
