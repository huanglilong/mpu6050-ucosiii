/*****************************************
*@file      : led.h
*@brief     : define led's API
*@time      : 2014/10/6
*@author    : jom
*******************************************/
#ifndef LED_H
#define LED_H

#include "stm32f4xx.h"

// led's function prototype
void ledInit(void);
void ledOn(void);
void ledOff(void);
void ledOn1(void);
void ledOff1(void);
void ledOn2(void);
void ledOff2(void);
void ledOn3(void);
void ledOff3(void);

#endif