/**********************************
* file          : app.c
* brief         : for MPU6050 IMU data fusion
* time          : 2015/7/30
* author        : huanglilong
**********************************/
#include  <includes.h>

static  OS_TCB   AppTaskStartTCB;
static  CPU_STK  AppTaskStartStk[APP_CFG_TASK_START_STK_SIZE];
                                                     
static  OS_TCB       App_TaskMPU6050TCB;                               /* task for read MPU6050 data */
static  CPU_STK      App_TaskMPU6050Stk[APP_CFG_TASK_MPU6050_STK_SIZE];

static  void  AppTaskStart          (void     *p_arg);
static  void  AppTaskCreate         (void);
static  void  AppObjCreate          (void);

static  void  App_TaskMPU6050       (void  *p_arg);

/* task entry */
int main(void)
{
    OS_ERR  err;
    
    BSP_IntDisAll();                                            /* Disable all interrupts.                              */
    CPU_Init();                                                 /* Initialize the uC/CPU Services                       */
    Mem_Init();                                                 /* Initialize Memory Management Module                  */
    Math_Init();                                                /* Initialize Mathematical Module                       */

    OSInit(&err);                                               /* Init uC/OS-III.                                      */

    OSTaskCreate((OS_TCB       *)&AppTaskStartTCB,              /* Create the start task                                */
                 (CPU_CHAR     *)"App Task Start",
                 (OS_TASK_PTR   )AppTaskStart,
                 (void         *)0u,
                 (OS_PRIO       )APP_CFG_TASK_START_PRIO,
                 (CPU_STK      *)&AppTaskStartStk[0u],
                 (CPU_STK_SIZE  )AppTaskStartStk[APP_CFG_TASK_START_STK_SIZE / 10u],
                 (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE,
                 (OS_MSG_QTY    )0u,
                 (OS_TICK       )0u,
                 (void         *)0u,
                 (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR       *)&err);

    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */

    (void)&err;

    return (0u);
}


/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

static  void  AppTaskStart (void *p_arg)
{
    OS_ERR  err;
    
   (void)p_arg;

    BSP_Init();                                                 /* Initialize BSP functions                             */
    BSP_Tick_Init();                                            /* Initialize Tick Services.                            */
    

#if OS_CFG_STAT_TASK_EN > 0u
    OSStatTaskCPUUsageInit(&err);                               /* Compute CPU capacity with no task running            */
#endif

#ifdef CPU_CFG_INT_DIS_MEAS_EN
    CPU_IntDisMeasMaxCurReset();
#endif

    APP_TRACE_DBG(("Creating Application Kernel Objects\n\r"));
    AppObjCreate();                                             /* Create Applicaiton kernel objects                    */

    APP_TRACE_DBG(("Creating Application Tasks\n\r"));
    AppTaskCreate();                                            /* Create Application tasks                             */

    while (DEF_TRUE) {                                          /* Task body, always written as an infinite loop.       */

        ledOn(); 
        OSTimeDlyHMSM(0u, 0u, 2u, 100u,
                      OS_OPT_TIME_HMSM_STRICT,
                      &err);
        ledOff();
        OSTimeDlyHMSM(0u, 0u, 2u, 100u,
                      OS_OPT_TIME_HMSM_STRICT,
                      &err);
    }
}


/*
*********************************************************************************************************
*                                          AppTaskCreate()
*
* Description : Create application tasks.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : AppTaskStart()
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  AppTaskCreate (void)
{
    OS_ERR  os_err;
    
    OSTaskCreate((OS_TCB      *)&App_TaskMPU6050TCB,
                 (CPU_CHAR    *)"MPU6050 driver",
                 (OS_TASK_PTR  ) App_TaskMPU6050, 
                 (void        *) 0,
                 (OS_PRIO      ) APP_CFG_TASK_MPU6050_PRIO,
                 (CPU_STK     *)&App_TaskMPU6050Stk[0],
                 (CPU_STK_SIZE ) App_TaskMPU6050Stk[APP_CFG_TASK_MPU6050_STK_SIZE / 10u],
                 (CPU_STK_SIZE ) APP_CFG_TASK_MPU6050_STK_SIZE,
                 (OS_MSG_QTY   ) 0u,
                 (OS_TICK      ) 0u,
                 (void        *) 0,
                 (OS_OPT       )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR | OS_OPT_TASK_SAVE_FP),
                 (OS_ERR      *)&os_err);
}


/*
*********************************************************************************************************
*                                          AppObjCreate()
*
* Description : Create application kernel objects tasks.
*
* Argument(s) : none
*
* Return(s)   : none
*
* Caller(s)   : AppTaskStart()
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  AppObjCreate (void)
{

}


void  App_TaskMPU6050 (void  *p_arg)
{
  OS_ERR  err;
  CPU_TS  ts;
  MPU6050_Acc_Gyro stCurrData;
  Mem_Set(&stCurrData, 0, sizeof(stCurrData));
  I2C_GPIO_Config();
  MPU6050_initialize();
  while (DEF_TRUE) 
  {
    OSTaskSemPend(10,
                  OS_OPT_PEND_BLOCKING,
                  &ts,
                  &err);
     if(err == OS_ERR_NONE)
     {
        MPU6050_getMotion6(&stCurrData);/* get MPU6050 Acc and Gyro data */
     }
    }
}
